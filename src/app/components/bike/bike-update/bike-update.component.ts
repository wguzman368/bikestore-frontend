import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BikeService } from '../bike.service';
import { ActivatedRoute, Router } from '@angular/router';
import { IBike } from '../bike';

@Component({
  selector: 'app-bike-update',
  templateUrl: './bike-update.component.html',
  styleUrls: ['./bike-update.component.css']
})
export class BikeUpdateComponent implements OnInit {


  formBikeUpdate: FormGroup;


  constructor(private formBuilder: FormBuilder,
              private bikeService: BikeService,
              private activateRoute: ActivatedRoute,
              private router: Router) {

    this.formBikeUpdate = this.formBuilder.group({
      id: [''],
      model: ['', [Validators.required, Validators.maxLength(4)]],
      serial: [''],
      price: ['']
    });

  }

  ngOnInit() {

    let id = this.activateRoute.snapshot.paramMap.get('id');
    console.warn('ID ', id);
    this.bikeService.getById(id)
.subscribe (res => {
       console.warn ('GET DATA ', res);
       this.loadForm(res);
    }, error => { });

  }

  private loadForm(bike: IBike) {
    this.formBikeUpdate.patchValue({
      id: bike.id,
      model: bike.model,
      price: bike.price,
      serial: bike.serial
    });
  }

  update(): void {
    this.bikeService.update(this.formBikeUpdate.value)
      .subscribe(res => {
        console.warn('UPDATE OK', res);
        this.router.navigate(['./bikes/bike-list']);
      }, error => {
        console.warn('Error', error);
      });
  }

}
