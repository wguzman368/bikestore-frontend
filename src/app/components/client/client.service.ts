import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IClient } from './client';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) { }
  public query(): Observable<IClient[]> {
    return this.http.get<IClient[]>(`${environment.END_POINT}/api/client`)
    .pipe(map( res => {
      return res;
  })); // end method query
}
}
