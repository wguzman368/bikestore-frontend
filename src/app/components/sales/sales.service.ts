import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable, from } from 'rxjs';

import { environment } from 'src/environments/environment';
import { IsalesCreate } from './salesCreate';

@Injectable({
  providedIn: 'root'
})
export class SalesService {


  constructor(private http: HttpClient) { }


public saveventa(sales: IsalesCreate): Observable<IsalesCreate> {
  return this.http.post<IsalesCreate>(`${environment.END_POINT}/api/sales`, sales)
  .pipe(map( res => {
    return res;
  }));

}
}





