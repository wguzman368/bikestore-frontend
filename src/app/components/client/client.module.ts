import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientViewComponent } from './client-view/client-view.component';
import { ClientCreateComponent } from './client-create/client-create.component';
import { ClientListComponent } from './client-list/client-list.component';
import { ClientUpdateComponent } from './client-update/client-update.component';
import { ClientRoutingModule } from './client-routing.module';




@NgModule({
  declarations: [ClientViewComponent, ClientCreateComponent, ClientListComponent, ClientUpdateComponent],
  imports: [
    CommonModule,
    ClientRoutingModule
  ]
})
export class ClientModule { }
