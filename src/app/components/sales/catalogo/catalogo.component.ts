import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-catalogo',
  templateUrl: './catalogo.component.html',
  styleUrls: ['./catalogo.component.css']
})
export class CatalogoComponent implements OnInit {

  searchForm = this.fb.group({
    document
  });
  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit() {
  }

}
