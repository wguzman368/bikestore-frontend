import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenuComponent } from './menu/menu.component';


const routes: Routes = [
  {
    path: 'bikes',
    loadChildren: () => import('./components/bike/bike.module')
    .then(m => m.BikeModule )
  },
  {
    path: 'client',
    loadChildren: () => import('./components/client/client.module')
    .then(m => m.ClientModule)
  },
  {
    path: 'sales',
    loadChildren: () => import('./components/sales/sales.module')
    .then(m => m.SalesModule)
  },
  {
    path: 'menu',
    component: MenuComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
