import { Component, OnInit } from '@angular/core';
import { BikeService } from '../bike.service';
import { IBike } from '../bike';

@Component({
  selector: 'app-bike-list',
  templateUrl: './bike-list.component.html',
  styleUrls: ['./bike-list.component.css']
})
export class BikeListComponent implements OnInit {

  bikeslist: IBike[];

  constructor(private bikeService: BikeService) { }

  ngOnInit() {
    this.bikeService.query()
    .subscribe((res:any) => {
      console.log('Get Data', res);
      this.bikeslist = res.content;
    }, error => {
    console.error('Error ', error);
  }
    );
  }

}
